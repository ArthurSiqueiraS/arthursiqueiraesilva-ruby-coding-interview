class TweetsController < ApplicationController
  def index
    tweets = Tweet.by_username(search_params[:user_username])

    render json: tweets
  end

  private

  def search_params
    params.permit(:user_username)
  end
end
