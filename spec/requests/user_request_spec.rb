require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)

        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching users by username' do
      let!(:company) { create(:company) }
      let!(:user_1) { create(:user, username: "User 10", company: company) }
      let!(:user_2) { create(:user, username: "User 20", company: company) }
      let!(:user_3) { create(:user, username: "User 30", company: company) }

      it 'returns only the users that fully match the specified username' do
        get company_users_path(company, { username: "User 20"})

        expect(result.first['id']).to eq(user_2.id)
        expect(result.first['username']).to eq(user_2.username)
      end

      it 'returns only the users that partially match the specified username' do
        get company_users_path(company, { username: "er 2"})

        expect(result.first['id']).to eq(user_2.id)
        expect(result.first['username']).to eq(user_2.username)
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      it 'returns all the users' do

      end
    end
  end
end
